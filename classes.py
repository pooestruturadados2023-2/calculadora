from os import system
import platform as pf

class Calculadora:
    def __init__(self):
        self.__registrador=float(0)
        self.__memoria=[]

    def somar(self,v1):
        '''
        Método que soma os valores e salva o resultado anterior na memória
        '''
        self.__memoria.append(self.__registrador)
        self.__registrador+=float(v1)

    def subtrair(self,v1):
        '''
        Método que subtrai os valores e salva o resultado anterior na memória
        '''
        self.__memoria.append(self.__registrador)
        self.__registrador=self.__registrador-float(v1)
    
    def dividir(self,v1):
        '''
        Método que verifica se o valor passado foi 0 e, caso contrário, realiza a subtração, salvando 
        o resultado anterior na memória
        '''
        if v1 != '0':
            self.__memoria.append(self.__registrador)
            self.__registrador=self.__registrador/float(v1)
        else:
            print('ERRO! Não é possível dividir por zero!')

    def multiplicar(self,v1):
        '''
        Método que multiplica os valores e salva o resultado anterior na memória
        '''
        if v1 != '0' and v1 != '-0': #intuito de impedir situacoes de -0.00
            self.__memoria.append(self.__registrador)
            self.__registrador=self.__registrador*float(v1)
        else:
            self.__memoria.append(self.__registrador)
            self.__registrador=float(0)

    def mostrar_registrador(self):
        '''
        Método que mostra o registrador
        '''
        return f'{self.__registrador:.2f}'
    
    def reset(self):
        '''
        Método que reseta a calculadora, limpando também os itens da memória
        '''
        self.__memoria=[]
        self.__registrador=float(0)

    def desfazer(self):
        '''
        Método que verifica se tem valor na memória e, caso positivo, recupera o valor recursivamente
        '''
        if len(self.__memoria) != 0:
            self.__registrador=self.__memoria.pop(len(self.__memoria)-1)
        else:
            print('Memória vazia!')

    def __str__(self):
        return f'''\
+--------------+
{self.mostrar_registrador()}
+--------------+
(+) somar
(-) subtrair
(/) dividir
(*) multiplicar
(r) resetar
(d) desfazer
---------------
'''

def clear():
    '''
    Função para limpar a tela compatível com windows
    '''
    if 'windows' in str(pf.platform()).lower():
        system('cls')
    else:
        system('clear')


def checa_numero(numero):
    '''
    Função para checar se a string passada é um número (inteiro [positivo ou negativo] ou float) incluindo os número com ',' ex: 9,8
    '''
    if str((numero).replace("-", "")).isnumeric == True:
        return True
    elif str((numero).replace("-", "")).replace(".", "").isnumeric() == True:
        return True
    elif str((numero).replace("-", "")).replace(",", "").isnumeric() == True:
        return True
    else:
        return False

def trata_numero(numero):
    '''
    Função para tratar caso um número float venha com "," ao invés de "."
    '''
    return str(numero).replace(",", ".")
