import classes

calculadora=classes.Calculadora()

classes.clear()
firstplay=True
while True:
    print(calculadora)
    if firstplay == True:
        vl=input('Digite um número... ')
        if classes.checa_numero(vl) != True:
            classes.clear()
            print('Digite um número!')
        else:
            classes.clear()
            calculadora.somar(classes.trata_numero(vl))
            if vl != '0' and vl != '-0':
                firstplay=False
    else:
        op=input('Operação: ')
        if op not in ['+','-','/','*','r','d']:
            classes.clear()
            print('Operação Inválida!')
        else:
            if op in ['r','d']:
                if op == 'r':
                    classes.clear()
                    calculadora.reset()
                    firstplay=True
                else:
                    classes.clear()
                    calculadora.desfazer()
            else:
                vl=input('Valor: ')
                if classes.checa_numero(vl) != True:
                    classes.clear()
                    print('Digite um número!')
                else:
                    if op == '+':
                        classes.clear()
                        calculadora.somar(classes.trata_numero(vl))
                    elif op == '-':
                        classes.clear()
                        calculadora.subtrair(classes.trata_numero(vl))
                    elif op == '*':
                        classes.clear()
                        calculadora.multiplicar(classes.trata_numero(vl))
                    elif op == '/':
                        classes.clear()
                        calculadora.dividir(classes.trata_numero(vl))